package main

import (
	"bytes"
	"fmt"
	"github.com/jinzhu/gorm"
	"imdb/models"
	"imdb/utils"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"runtime"
	"strings"
	"testing"
	"time"
)

var db *gorm.DB

func TestMain(m *testing.M) {
	// Before all tests
	db = utils.DbManager()
	ensureTablesExist()
	utils.FlushDB()
	code := m.Run()
	// After all tests
	utils.FlushDB()
	os.Exit(code)
}

// Test that no movies are returned when db is empty.
func TestMovieSearchServiceEmptySearchResultsWithNoData(t *testing.T) {
	beforeTest()
	response, _ := http.Get("http://movie-search-service:3000/search")

	checkResponseCode(t, http.StatusOK, response.StatusCode)

	bodyBytes, _ := ioutil.ReadAll(response.Body)
	if body := strings.TrimSpace(string(bodyBytes)); body != "[]" {
		fmt.Println(body)
		t.Errorf("Expected an empty array. Got %s", body)
	}
	afterTest()
}

// Tests that no movies are returns for default year (when it has no movies)
func TestMovieSearchServiceEmptySearchResultWithMovies(t *testing.T) {
	beforeTest()
	createMoviesActorsGenresDirectors(false)
	// By default 2016 is searched
	response, _ := http.Get("http://movie-search-service:3000/search")

	checkResponseCode(t, http.StatusOK, response.StatusCode)

	bodyBytes, _ := ioutil.ReadAll(response.Body)
	if body := strings.TrimSpace(string(bodyBytes)); body != "[]" {
		fmt.Println(body)
		t.Errorf("Expected an empty array. Got %s", body)
	}
	afterTest()
}

// Tests that movies are returned for default year (when it has movies)
func TestMovieSearchServiceDefaultSearchResultWithMovies(t *testing.T) {
	beforeTest()
	createMoviesActorsGenresDirectors(true)
	// By default 2016 is searched
	response, _ := http.Get("http://movie-search-service:3000/search")

	checkResponseCode(t, http.StatusOK, response.StatusCode)

	bodyBytes, _ := ioutil.ReadAll(response.Body)
	body := strings.TrimSpace(string(bodyBytes))
	assertStringsFound(body, []string{"The Bfg"}, t)
	afterTest()
}

// Tests that not results are returned for a year with no movies.
func TestMovieSearchServiceNoSearchResultWithYear(t *testing.T) {
	beforeTest()
	createMoviesActorsGenresDirectors(false)
	response, _ := http.Get("http://movie-search-service:3000/search?years=2015")

	checkResponseCode(t, http.StatusOK, response.StatusCode)

	bodyBytes, _ := ioutil.ReadAll(response.Body)
	if body := strings.TrimSpace(string(bodyBytes)); body != "[]" {
		fmt.Println(body)
		t.Errorf("Expected an empty array. Got %s", body)
	}
	afterTest()
}

// Tests that search works with a year with movies.
func TestMovieSearchServiceSearchResultWithMoviesWithYear(t *testing.T) {
	beforeTest()
	createMoviesActorsGenresDirectors(true)
	// By default 2016 is searched
	response, _ := http.Get("http://movie-search-service:3000/search?years=1995")

	checkResponseCode(t, http.StatusOK, response.StatusCode)

	bodyBytes, _ := ioutil.ReadAll(response.Body)
	body := strings.TrimSpace(string(bodyBytes))
	assertStringsFound(body, []string{"Se7en", "Scream"}, t)
	afterTest()
}

// Tests that search works with a year range
func TestMovieSearchServiceSearchResultWithMoviesWithYearRange(t *testing.T) {
	beforeTest()
	createMoviesActorsGenresDirectors(true)
	// By default 2016 is searched
	response, _ := http.Get("http://movie-search-service:3000/search?years=1993-1995")

	checkResponseCode(t, http.StatusOK, response.StatusCode)

	bodyBytes, _ := ioutil.ReadAll(response.Body)
	body := strings.TrimSpace(string(bodyBytes))
	assertStringsFound(body, []string{"Jurassic Park", "Se7en", "Scream"}, t)
	afterTest()
}

// Tests that the parse csv service works.
func TestParseCSVService(t *testing.T) {
	beforeTest()
	_ = utils.ProduceMessage("test-assets/smaller-valid-file.csv", "csv_uploaded")

	// Ensure we have data in db by this point.
	time.Sleep(5 * time.Second)
	var movie models.Movie
	var director models.Director
	var actor models.Actor
	var genre models.Genre

	if db.First(&movie).RecordNotFound() {
		t.Errorf("Expected at least one movie")
	}
	if db.First(&director).RecordNotFound() {
		t.Errorf("Expected at least one movie")
	}
	if db.First(&actor).RecordNotFound() {
		t.Errorf("Expected at least one movie")
	}
	if db.First(&genre).RecordNotFound() {
		t.Errorf("Expected at least one movie")
	}
	afterTest()
}

func TestUploadCsvServiceInvalidFileType(t *testing.T) {
	beforeTest()
	response, err := uploadFile("test-assets/earth-moon-image.jpg", "earth-moon-image.jpg", t)
	if err != nil {
		t.Error(err)
	}
	checkResponseCode(t, http.StatusBadRequest, response.StatusCode)

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		t.Error(err)
	}

	bodyString := strings.TrimSpace(string(body))
	if !strings.Contains(bodyString, "Invalid file type") {
		t.Errorf("Incorrect status: expected %s", bodyString)
	}
	afterTest()
}

func TestUploadCsvServiceInvalidTextFile(t *testing.T) {
	beforeTest()
	response, err := uploadFile("test-assets/some-invalid-file.txt", "some-invalid-file.txt", t)
	if err != nil {
		t.Error(err)
	}
	checkResponseCode(t, http.StatusBadRequest, response.StatusCode)

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		t.Error(err)
	}

	bodyString := strings.TrimSpace(string(body))
	if !strings.Contains(bodyString, "Invalid csv data") {
		t.Errorf("Incorrect status: expected %s", bodyString)
	}
	afterTest()
}

func TestUploadCsvServiceInvalidCsvFile(t *testing.T) {
	beforeTest()
	response, err := uploadFile("test-assets/some-invalid-csv.csv", "some-invalid-csv.csv", t)
	if err != nil {
		t.Error(err)
	}
	checkResponseCode(t, http.StatusBadRequest, response.StatusCode)

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		t.Error(err)
	}

	bodyString := strings.TrimSpace(string(body))
	if !strings.Contains(bodyString, "Invalid csv data") {
		t.Errorf("Incorrect status: expected %s", bodyString)
	}
	afterTest()
}

func TestUploadCsvServiceValidCsvFile(t *testing.T) {
	beforeTest()
	time.Sleep(5 * time.Second)
	response, err := uploadFile("test-assets/smaller-valid-file.csv", "smaller-valid-file.csv", t)
	if err != nil {
		t.Error(err)
	}
	checkResponseCode(t, http.StatusOK, response.StatusCode)

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		t.Error(err)
	}

	bodyString := strings.TrimSpace(string(body))
	if !strings.Contains(bodyString, "OK - queued for processing") {
		t.Errorf("Incorrect status: expected %s", bodyString)
	}

	// Ensure we have data in db by this point.
	time.Sleep(5 * time.Second)
	var movie models.Movie
	var director models.Director
	var actor models.Actor
	var genre models.Genre

	if db.First(&movie).RecordNotFound() {
		t.Errorf("Expected at least one movie")
	}
	if db.First(&director).RecordNotFound() {
		t.Errorf("Expected at least one movie")
	}
	if db.First(&actor).RecordNotFound() {
		t.Errorf("Expected at least one movie")
	}
	if db.First(&genre).RecordNotFound() {
		t.Errorf("Expected at least one movie")
	}

	afterTest()
}

////////// HELPER FUNCTIONS //////////

func beforeTest() {
	pc, _, _, ok := runtime.Caller(1)
	details := runtime.FuncForPC(pc)
	if ok && details != nil {
		log.Printf("Running %s\n", details.Name())
	}
	ensureTablesExist()
	utils.FlushDB()
}

func afterTest() {
	utils.FlushDB()
}

func ensureTablesExist() {
	tables := []string{"movies", "directors", "actors", "genres", "movie_actors", "movie_genre"}
	for _, table := range tables {
		tableQuery := fmt.Sprintf("SELECT 1 FROM %s", table)
		if _, err := db.Raw(tableQuery).Rows(); err != nil {
			log.Fatal(err)
		}
	}
}

func checkResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}

func assertStringsFound(body string, substrings []string, t *testing.T) {
	for _, substring := range substrings {
		if !strings.Contains(body, substring) {
			t.Errorf("Expected string: '%s'. Got %s", substring, body)
		}
	}
}

func createMoviesActorsGenresDirectors(includeDefaultMovie bool) {
	// I know this is not programmatic but was the easiest way to be able to see various types of results.

	directorSpielberg := models.Director{Name: "Steven Spielberg"}
	directorFincher := models.Director{Name: "David Fincher"}
	directorWesCraven := models.Director{Name: "Wes Craven"}
	db.Save(&directorSpielberg)
	db.Save(&directorFincher)
	db.Save(&directorWesCraven)

	genreComedy := models.Genre{Name: "Comedy"}
	genreThriller := models.Genre{Name: "Thriller"}
	genreActionAdventure := models.Genre{Name: "Action Adventure"}
	genreDrama := models.Genre{Name: "Drama"}
	genreHorror := models.Genre{Name: "Horror"}
	db.Save(&genreComedy)
	db.Save(&genreThriller)
	db.Save(&genreActionAdventure)
	db.Save(&genreDrama)
	db.Save(&genreHorror)

	actorBradPitt := models.Actor{Name: "Brad Pitt"}
	actorMorganFreeman := models.Actor{Name: "Morgan Freeman"}
	actorGwynethPaltrow := models.Actor{Name: "Gwyneth Paltrow"}
	actorJeffGoldblum := models.Actor{Name: "Jeff Goldblum"}
	actorLauraDern := models.Actor{Name: "Laura Dern"}
	actorHenryThomas := models.Actor{Name: "Henry Thomas"}
	actorDrewBarrymore := models.Actor{Name: "Drew Barrymore"}
	db.Save(&actorBradPitt)
	db.Save(&actorMorganFreeman)
	db.Save(&actorGwynethPaltrow)
	db.Save(&actorJeffGoldblum)
	db.Save(&actorLauraDern)
	db.Save(&actorHenryThomas)
	db.Save(&actorDrewBarrymore)

	movieET := models.Movie{
		Title:         "ET",
		Description:   "Story of a boy who meets an Alien",
		DirectorRefer: directorSpielberg.ID,
		Year:          1982,
		Runtime:       120,
		Rating:        8.5,
		Votes:         300000,
		Revenue:       141.8,
		Metascore:     88,
	}
	db.Save(&movieET)
	movieET.UpdateMovieActors(db, []models.Actor{actorHenryThomas, actorDrewBarrymore})
	movieET.UpdateMovieGenres(db, []models.Genre{genreActionAdventure, genreComedy})

	movieSeven := models.Movie{
		Title:         "Se7en",
		Description:   "A gruesome murder mystery",
		DirectorRefer: directorFincher.ID,
		Year:          1995,
		Runtime:       120,
		Rating:        8.6,
		Votes:         1000000,
		Revenue:       327.3,
		Metascore:     85,
	}
	db.Save(&movieSeven)
	movieSeven.UpdateMovieActors(db, []models.Actor{actorBradPitt, actorMorganFreeman, actorGwynethPaltrow})
	movieET.UpdateMovieGenres(db, []models.Genre{genreDrama, genreThriller})

	movieJurassicPark := models.Movie{
		Title:         "Jurassic Park",
		Description:   "A park adventure gone gone wrong",
		DirectorRefer: directorSpielberg.ID,
		Year:          1993,
		Runtime:       120,
		Rating:        8.1,
		Votes:         300000,
		Revenue:       1002.9,
		Metascore:     68,
	}
	db.Save(&movieJurassicPark)
	movieJurassicPark.UpdateMovieActors(db, []models.Actor{actorJeffGoldblum, actorLauraDern})
	movieET.UpdateMovieGenres(db, []models.Genre{genreActionAdventure, genreThriller})

	movieScream := models.Movie{
		Title:         "Scream",
		Description:   "A warm tale of romance",
		DirectorRefer: directorWesCraven.ID,
		Year:          1995,
		Runtime:       120,
		Rating:        7.2,
		Votes:         300000,
		Revenue:       173,
		Metascore:     65,
	}
	db.Save(&movieScream)
	movieScream.UpdateMovieActors(db, []models.Actor{actorDrewBarrymore})
	movieScream.UpdateMovieGenres(db, []models.Genre{genreHorror})

	if includeDefaultMovie {
		movieBfg := models.Movie{
			Title:         "The Bfg",
			Description:   "A story about a giant and his human friend",
			DirectorRefer: directorSpielberg.ID,
			Year:          2016,
			Runtime:       117,
			Rating:        7.5,
			Votes:         10000,
			Revenue:       183.3,
			Metascore:     68,
		}
		db.Save(&movieBfg)
		movieScream.UpdateMovieActors(db, []models.Actor{actorJeffGoldblum})
		movieScream.UpdateMovieGenres(db, []models.Genre{genreActionAdventure})
	}
}

func uploadFile(filepath string, filename string, t *testing.T) (*http.Response, error) {
	// Open the file
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatalln(err)
	}
	// Close the file later
	defer file.Close()

	// Buffer to store our request body as bytes
	var requestBody bytes.Buffer

	// Create a multipart writer
	multiPartWriter := multipart.NewWriter(&requestBody)

	// Initialize the file field
	fileWriter, err := multiPartWriter.CreateFormFile("csv", filename)
	if err != nil {
		t.Error(err)
	}

	// Copy the actual file content to the field field's writer
	_, err = io.Copy(fileWriter, file)
	if err != nil {
		t.Error(err)
	}

	// We completed adding the file and the fields, let's close the multipart writer
	// So it writes the ending boundary
	_ = multiPartWriter.Close()

	// By now our original request body should have been populated, so let's just use it with our custom request
	req, err := http.NewRequest("POST", "http://upload-csv-service:3000/upload", &requestBody)
	if err != nil {
		t.Error(err)
	}
	// We need to set the content type from the writer, it includes necessary boundary as well
	req.Header.Set("Content-Type", multiPartWriter.FormDataContentType())

	// Do the request
	client := &http.Client{}
	return client.Do(req)
}
