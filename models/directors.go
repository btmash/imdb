package models

import (
	"github.com/jinzhu/gorm"
)

type Director struct {
	gorm.Model
	Name   string   `gorm:"not null;unique_index;"`
	Movies []*Movie `gorm:"foreignkey:DirectorRefer;"`
}

// Returns Director object from string.
// If the director does not exist in the db, then they are created.
func GetDirectorFromString(director string, db *gorm.DB) Director {
	var directorObject Director
	db.FirstOrCreate(&directorObject, Director{Name: director})
	return directorObject
}
