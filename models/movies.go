package models

import "github.com/jinzhu/gorm"

type Movie struct {
	gorm.Model
	Title         string   `gorm:"type:varchar(255);not null;default '';unique_index:title_director_year;"`
	Description   string   `gorm:"type:text;"`
	Actors        []Actor  `gorm:"many_to_many:movie_actors;"`
	Genres        []Genre  `gorm:"many_to_many:movie_genre;"`
	Director      Director `gorm:"foreignkey:DirectorRefer;"`
	DirectorRefer uint     `gorm:"not null; default 0; unique_index:title_director_year;"`
	Year          int      `gorm:"not null; default 0; unique_index:title_director_year;index:rating_year;"`
	Runtime       int
	Rating        float32 `gorm:"not null; default 0;index:rating_year;"`
	Votes         int
	Revenue       float32
	Metascore     int
}

// Adds relationship between movie and genre into table.
// This was done because gorm's auto association was not working as expected. In an effort to get this done, the
// necessary db calls were done to create a relationship between a movie and the genres it belongs to.
func (movie Movie) UpdateMovieGenres(db *gorm.DB, genres []Genre) bool {
	db.Exec("DELETE FROM movie_genre WHERE movie_id = ?", movie.ID)
	for _, genre := range genres {
		db.Exec("INSERT INTO movie_genre(genre_id, movie_id) VALUES(?, ?)", genre.ID, movie.ID)
	}
	return true
}

// Adds relationship between movie and actor into table.
// This was done because gorm's auto association was not working as expected. In an effort to get this done, the
// necessary db calls were done to create a relationship between a movie and the actors it belongs to.
func (movie Movie) UpdateMovieActors(db *gorm.DB, actors []Actor) bool {
	db.Exec("DELETE FROM movie_actors WHERE movie_id = ?", movie.ID)
	for _, actor := range actors {
		db.Exec("INSERT INTO movie_actors(actor_id, movie_id) VALUES(?, ?)", actor.ID, movie.ID)
	}
	return true
}
