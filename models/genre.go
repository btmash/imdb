package models

import (
	"github.com/jinzhu/gorm"
	"strings"
)

type Genre struct {
	gorm.Model
	Name   string   `gorm:"not null;unique_index;"`
	Movies []*Movie `gorm:"many2many:movie_genre;"`
}

// Returns slice of Genre objects from string
// If the genre does not exist in the db, then it is created.
func GetGenresFromString(genres string, db *gorm.DB) []Genre {
	genresSplit := strings.Split(genres, ",")
	var genreSlice []Genre
	for _, genre := range genresSplit {
		var genreObject Genre
		db.FirstOrCreate(&genreObject, Genre{Name: strings.TrimSpace(genre)})
		genreSlice = append(genreSlice, genreObject)
	}
	return genreSlice
}
