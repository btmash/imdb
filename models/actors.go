package models

import (
	"github.com/jinzhu/gorm"
	"strings"
)

type Actor struct {
	gorm.Model
	Name   string   `gorm:"not null;unique_index;"`
	Movies []*Movie `gorm:"many2many:movie_actors;"`
}

// Returns slice of Actor objects from string
// If the actors do not exist in the db, then they are created.
func GetActorsFromString(actors string, db *gorm.DB) []Actor {
	actorsSplit := strings.Split(actors, ",")
	var actorSlice []Actor
	for _, actor := range actorsSplit {
		var actorObject Actor
		db.FirstOrCreate(&actorObject, Actor{Name: strings.TrimSpace(actor)})
		actorSlice = append(actorSlice, actorObject)
	}
	return actorSlice
}
