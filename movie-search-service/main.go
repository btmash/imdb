package main

import (
	"encoding/json"
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/joho/godotenv"
	"github.com/julienschmidt/httprouter"
	"imdb/models"
	"imdb/utils"
	"log"
	"net/http"
	"strconv"
	"strings"
)

const DefaultYear = "2016"

type imdbResponse struct {
	Title       string   `json:"title"`
	Genres      []string `json:"genres"`
	Year        int      `json:"year"`
	Rating      float32  `json:"rating"`
	Runtime     int      `json:"runtime"`
	Description string   `json:"description"`
}

func init() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
}

func main() {
	r := httprouter.New()
	r.GET("/search", searchMovies)
	_ = http.ListenAndServe(":3000", r)
}

// Search through the db in the movies.
func searchMovies(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	response := make([]imdbResponse, 0)

	queryValues := r.URL.Query()
	years := queryValues.Get("years")
	genres := queryValues.Get("genres")
	db := utils.DbManager()
	tx := db.Table("movies").Select("DISTINCT(movies.id)")
	if years == "" {
		years = DefaultYear
	}
	tx = createYearTx(years, tx)
	// The auto association from gorm was not working as expected so a series of joins are necessary to get this
	// function to show up properly.
	if genres != "" {
		tx = tx.Joins("INNER JOIN movie_genre mg ON movies.id = mg.movie_id")
		tx = tx.Joins("INNER JOIN genres g ON g.id = mg.genre_id")
		genresSplit := strings.Split(genres, ",")
		for k, v := range genresSplit {
			genresSplit[k] = strings.TrimSpace(v)
		}
		tx = tx.Where("g.name IN (?)", genresSplit)
	}
	tx = tx.Order("movies.rating DESC")
	rows, _ := tx.Rows()
	for rows.Next() {
		var movie models.Movie
		var movieGenres []string
		var id int
		_ = rows.Scan(&id)
		db.Where("id = ?", id).First(&movie)
		// Since the gorm association is not working, this shows how to get the genres a movie belongs to
		// (would be happening behind the scenes otherwise).
		genreRows, _ := db.Raw("SELECT g.name FROM genres g INNER JOIN movie_genre mg ON g.id = mg.genre_id WHERE mg.movie_id = ?", movie.ID).Rows()
		for genreRows.Next() {
			var movieGenre string
			_ = genreRows.Scan(&movieGenre)
			movieGenres = append(movieGenres, movieGenre)
		}
		response = append(response, imdbResponse{
			Title:       movie.Title,
			Year:        movie.Year,
			Genres:      movieGenres,
			Rating:      movie.Rating,
			Runtime:     movie.Runtime,
			Description: movie.Description,
		})
	}
	_ = json.NewEncoder(w).Encode(response)
}

// Creates a new tx object with the year related queries appended to it.
// The `if` blocks could probably be cleaned up but it makes assumptions about a valid use case.
func createYearTx(years string, tx *gorm.DB) *gorm.DB {
	if strings.Contains(years, "-") {
		splitYears := strings.Split(years, "-")
		fmt.Println(splitYears)
		if len(splitYears) > 2 {
			return tx.Where("movies.year = ?", DefaultYear)
		}
		startYear, err := strconv.Atoi(splitYears[0])
		if err != nil {
			return tx.Where("movies.year = ?", DefaultYear)
		}
		endYear, err := strconv.Atoi(splitYears[1])
		if err != nil {
			return tx.Where("movies.year = ?", DefaultYear)
		}
		if startYear > endYear {
			startYear, endYear = endYear, startYear
		}
		tx = tx.Where("movies.year >= ?", startYear)
		tx = tx.Where("movies.year <= ?", endYear)
		return tx
	}
	return tx.Where("movies.year = ?", years)
}
