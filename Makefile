init:
	cp -f .env.example .env

start:
	docker-compose up

down:
	docker-compose down

migrate:
	docker-compose exec app-cli-service go run migrations/main.go

flushdb:
	docker-compose exec app-cli-service go run flush-db-service/flush-db.go

restart-app-services:
	docker-compose restart upload-csv-service parse-csv-service movie-search-service app-cli-service

test:
	docker-compose exec app-cli-service go test

clean:
	rm -f uploaded-csv/*.csv \

uninstall:
	docker-compose down \
		&& rm -f uploaded-csv/*.csv
