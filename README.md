# Go IMDB Go

This is a sample set of services written using Go. Note that I've never written
something this fleshed out in Go before so there are likely a large number of
improvements that could be made to the code.

A large part of how this codebase was put together is inspired (VERY STRONGLY)
by Guillerme Kuhn and his event driven go codebase at https://github.com/gkuhn1/event-driven-go

## Installing

To install, you first need to have docker installed on your machine and it
needs to support using Makefiles. So we would run things in this order at
the terminal:

- `make init` - copies .env.example to .env (or do this manually)
- `make install` - This just makes sure the `mysql_data` directory
- `make start` - This starts the docker containers. Note that they are running in the terminal
as I wanted you to be able to see the output from the containers.
- Start up a new terminal
- `make migrate` - This will create all the tables in the mariadb container
database.
- `make test` - This will run the various tests that have been written out in
`integration_test.go`
- Upload csv for your own test. You can access the upload service at
`http://localhost:3000/upload` to upload a csv file. I have provided various
files in the `test-assets` directory that you can use yourself. Given that this
was a backend test, I did not provide a user-facing form. You can create
a `multipart/form-data` POST request and attach a file with the key `csv`.
This is easiest done using postman.
- Browse the json results. You can access the movie service at
`http://localhost:3001/search` to search for movies (with the default search
year being 2016). To search by year (or years), you can use the `years` key
so you have `http://localhost:3001/search?years=2010` for a single year or
`http://localhost:3001/search?years=2010-2012` for movies from 2010-2012
(both start and end years included). You can also search by genre(s) so we
have `http://localhost:3001/search?genres=Comedy` or multiple genres with
`http://localhost:3001/search?genres=Comedy,Horror` so it will include movies
from either genre. Finally, we can combine the above so you could do
`http://localhost:3001/search?years=2010-2012&genres=Comedy,Horror` so you
get movies from 2010-2012 that were comedy or horror (or both) movies.
- `make clean` - just cleans up some of uploaded csv files from the
`uploaded-csv` directory.
- `make restart-app-services` - sometimes there are issues between kafka
and the web services (this happens when services are brought down and back up).
This will restart the go services and then the queue service should work
properly again after. 
- `make down` - to bring down and remove the docker container.
- `make uninstall` - brings down docker container, removes csv files and
the `mysql_data` directory.

## Upload service

- `http://localhost:3000/upload`
This is where files get uploaded. Since it is a multipart form, the key to
use is `csv`. You can see how this is in the `integration_test.go`

## Sample search queries

- `http://localhost:3001/search?years=3000`
- `http://localhost:3001/search`
- `http://localhost:3001/search?years=2010`
- `http://localhost:3001/search?years=2010-2012`
- `http://localhost:3001/search?genres=Comedy`
- `http://localhost:3001/search?years=2010-2012&genres=Comedy,Horror`

## Structure

- `flush-db-service`
This is just a service to flush the tables in the db used by the services.

- `migrations`
The gorm migrations are run here to create the various tables.

- `models`
The various models used in the database. This consists of `Movies`,
`Directors`, `Actors` and `Genres`. I opted to make directors their own model
so we could query by them in the future if we wanted. Movies can have multiple
actors (and vice versa) along with genres so they have many-to-many
relationship tables (`movie_actors` and `movie_genre` respectively.)
The associations were not working the way I expected so I rolled by own
to recreate the associations after a movie is saved.

- `movie-search-service`
This is the endpoint for searching movies

- `upload-csv-service`
This is the endpoint for uploading csv files. When they are uploaded, they
are first validated to be csv files that conform to the assignment
csv params (12 columns of data in text/csv or text/plain mimetype). Once
validated, it is queued for processing by `parse-csv-service.`

- `parse-csv-service`
I didn't want the upload service to get tied up in processing lots of data
in the csv so I created a intermediary service to parse the csv so this can
take its time to process a file.

`test-assets` and `uploaded-csv` are mostly there to see assets get uploaded,
and for any test assets you may wish to use.

`utils` contains classes to connect to the database, and to produce and consume
kafka messages which is explained below.

## Architecture
This has **attempted** to follow a microservices-esque architecture. The main
idea was:

- Upload csv (`upload-csv-service`) ->
- Process csv (`parse-csv-service`)
- Search endpoint (service)

The communication is done using Kafka (with a `csv_uploaded` message along
with a path to the file.). The uploader produces the message and `parse-csv-service`
receives the message (and path to file) which it will begin to process.
Upload and search could potentially have been the same service file but
separating this out to multiple endpoints made it easier to think about in
in the end. And if we wanted to combine them, then it could easily be put back
together in one file. 

## Things I wish I did better

- I don't fully understand everything using `golang` so my directory structure
could probably be improved significantly for the non-services and the tests.
- My issues with gorm required me to do a workaround
- Using postgres might have been a better thing in hindsight I believe my gorm
issues may be partially related to that and I also ran into weird connection
errors. My lack of knowledge in debugging postgres errors is what led me in
this direction, however.
- I'm still unsure if the two user-facing endpoints should have been available
from one port. It would certainly make things easier. The pro is separate code,
more easily separated tests (if I had done that) and easier to secure certain
services behind firewalls if need be. The con is needing to know to visit a 
separate url/port to get things done.
- I wish I knew how to structure separated tests better. Right now it is in
one file which makes it difficult to grok on which tests are for which service.

![Speed Racer Image](https://m.media-amazon.com/images/M/MV5BZmI4MzQ1OWMtYjNhNi00MmMzLWFhMTUtNGY0NDE3ZDQyMWY0XkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_SY1000_CR0,0,733,1000_AL_.jpg)