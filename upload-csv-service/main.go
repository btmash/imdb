package main

import (
	"bufio"
	"encoding/csv"
	"github.com/joho/godotenv"
	"github.com/julienschmidt/httprouter"
	"imdb/utils"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
)

func init() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
}

func main() {
	r := httprouter.New()
	r.POST("/upload", uploadCSV)
	_ = http.ListenAndServe(":3000", r)
}

func uploadCSV(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")

	r.Body = http.MaxBytesReader(w, r.Body, 10<<20+512)
	err := r.ParseMultipartForm(10 << 20)
	// Out of bounds file size
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	file, _, err := r.FormFile("csv")
	// csv key does not have file
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	defer file.Close()
	tempFile, err := ioutil.TempFile("uploaded-csv", "upload-*.csv")
	// Could not create file for some reason
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	defer tempFile.Close()
	fileBytes, err := ioutil.ReadAll(file)
	// Cannot read file - possibly invalid
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Ensure we only deal with csv types
	filetype := http.DetectContentType(fileBytes)
	if !strings.Contains(filetype, "text/plain") && !strings.Contains(filetype, "text/csv") {
		_ = os.Remove(tempFile.Name())
		http.Error(w, `{"error":"Invalid file type"}`, http.StatusBadRequest)
		return
	}

	// write this byte array to our temporary file
	_, err = tempFile.Write(fileBytes)
	if err != nil {
		_ = os.Remove(tempFile.Name())
		http.Error(w, `{"error":"Invalid file type"}`, http.StatusBadRequest)
		return
	}

	// Ensure csv has the right number of columns
	csvFile, _ := os.Open(tempFile.Name())
	reader := csv.NewReader(bufio.NewReader(csvFile))
	for {
		record, err := reader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}

		if len(record) != 12 {
			_ = os.Remove(tempFile.Name())
			http.Error(w, `{"error":"Invalid csv data"}`, http.StatusBadRequest)
			return
		}
	}
	_ = utils.ProduceMessage(tempFile.Name(), "csv_uploaded")

	w.WriteHeader(200)
	_, _ = w.Write([]byte(`{"result":"OK - queued for processing"}`))
}
