CREATE TABLE IF NOT EXISTS directors (
  id INT(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key.',
  name VARCHAR (100) NOT NULL DEFAULT '' COMMENT 'Director name.',
  PRIMARY KEY(id),
  KEY(name),
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Director Information Table.';;

CREATE TABLE IF NOT EXISTS director_movies (
  director_id INT(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Director ID from directors table.',
  movie_id INT(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Movie ID FROM movies table.',
  UNIQUE KEY (movie_id, director_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Director-Movie Relationship';
