CREATE TABLE IF NOT EXISTS actors (
  id INT(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key.',
  name VARCHAR (100) NOT NULL DEFAULT '' COMMENT 'Actor name.',
  PRIMARY KEY(id),
  KEY(name),
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Actor Information Table.';

CREATE TABLE IF NOT EXISTS actors_movies (
  actor_id INT(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Actor ID FROM actors table.',
  movie_id INT(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Movie ID FROM movies table.',
  UNIQUE KEY (movie_id, actor_id)
  KEY actor_movie(actor_id, movie_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Actor-Movie relationship';
