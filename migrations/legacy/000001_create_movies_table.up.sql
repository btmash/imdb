CREATE TABLE IF NOT EXISTS movies (
  id INT(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key.',
  title VARCHAR(255) NOT NULL DEFAULT '' COMMENT 'Movie TItle.',
  description TEXT COMMENT 'Movie Description.',
  director INT(20) NOT NULL DEFAULT '' COMMENT 'Movie Director.',
  year SMALLINT unsigned NOT NULL DEFAULT 0 COMMENT 'Year movie was made.',
  runtime SMALLINT unsigned NOT NULL DEFAULT 0 COMMENT 'Runtime in minutes.',
  rating DECIMAL(3,1) NOT NULL DEFAULT 0.0 COMMENT 'Audience rating.',
  votes INT unsigned NOT NULL DEFAULT 0 COMMENT 'Number of votes.',
  revenue DECIMAL(6,2) NOT NULL DEFAULT 0 COMMENT 'Revenue in millions.',
  metascore TINYINT unsigned NOT NULL DEFAULT 0 COMMENT 'Metascore (0-100).',
  PRIMARY KEY(id),
  UNIQUE KEY (title, director, year),
  KEY year (year)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Movie Information Table.';
