package main

import (
	"fmt"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"imdb/models"
	"imdb/utils"
)

func main() {
	db := utils.DbManager()
	defer db.Close()
	db.LogMode(true)
	db.AutoMigrate(&models.Director{}, &models.Movie{}, &models.Actor{}, &models.Genre{})
	fmt.Println("Migrations complete")
}
