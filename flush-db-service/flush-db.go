package main

import (
	"fmt"
	"imdb/utils"
)

func main() {
	defer utils.DbManager().Close()
	utils.FlushDB()
	fmt.Println("Tables cleared")
}
