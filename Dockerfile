# Dockerfile References: https://docs.docker.com/engine/reference/builder/

# Start from golang:latest base image
FROM golang:latest

ENV PKG_CONFIG_PATH=/usr/lib/pkgconfig
ENV LD_LIBRARY_PATH=/usr/lib

# Add Maintainer Info
LABEL maintainer="Ashok Modi"

RUN \
    git clone https://github.com/edenhill/librdkafka.git && \
    cd librdkafka && \
    ./configure --prefix /usr && \
    make && \
    make install

# Set the Current Working Directory inside the container
WORKDIR /app

# Copy go mod and sum files
COPY go.mod go.sum ./

# Download all dependancies. Dependencies will be cached if the go.mod and go.sum files are not changed
RUN go mod download
