package main

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/joho/godotenv"
	"github.com/julienschmidt/httprouter"
	"imdb/models"
	"imdb/utils"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
)

func init() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
}

func main() {
	go initConsumer()

	fmt.Println("Running parse-csv-service")

	r := httprouter.New()
	r.POST("/parse", handler)
	_ = http.ListenAndServe(":3000", r)
}

func initConsumer() {
	_ = utils.Consume([]string{"csv_uploaded"}, "parse-csv-service-consumer-events", callback)
}

func callback(m *kafka.Message) {
	filename := string(m.Value)
	parseCsvFile(filename)
}

func handler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	return
}

func parseCsvFile(filename string) {
	csvFile, _ := os.Open(filename)
	reader := csv.NewReader(bufio.NewReader(csvFile))
	db := utils.DbManager()
	skipFirst := true
	for {
		record, err := reader.Read()
		if skipFirst {
			skipFirst = false
			continue
		}
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}

		// Skip record if first column is empty
		if record[0] == "" {
			continue
		}
		director := models.GetDirectorFromString(record[4], db)
		genres := models.GetGenresFromString(record[2], db)
		actors := models.GetActorsFromString(record[5], db)
		rating, _ := strconv.ParseFloat(record[8], 32)
		revenue, _ := strconv.ParseFloat(record[10], 32)
		year, _ := strconv.Atoi(record[6])

		var movie models.Movie
		db.FirstOrCreate(&movie, models.Movie{Title: record[1], DirectorRefer: director.ID, Year: year})
		movie.Description = record[3]
		movie.Runtime, _ = strconv.Atoi(record[7])
		movie.Rating = float32(rating)
		movie.Votes, _ = strconv.Atoi(record[9])
		movie.Revenue = float32(revenue)
		movie.Metascore, _ = strconv.Atoi(record[11])
		fmt.Println(movie)
		db.Save(&movie)
		movie.UpdateMovieActors(db, actors)
		movie.UpdateMovieGenres(db, genres)
		// I could not get the associations working the way I would have liked hence the above approach.
		// @TODO: Fix up associations so they work correctly
		// db.Model(&movie).Association("Genres").Replace(genres)
		// db.Model(&movie).Association("Actors").Replace(actors)
	}
}
