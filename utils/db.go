package utils

import (
	"fmt"
	"github.com/joho/godotenv"
	"log"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var db *gorm.DB

func init() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	mysqlConnectString := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=true", os.Getenv("MYSQL_USERNAME"), os.Getenv("MYSQL_PASSWORD"), os.Getenv("MYSQL_HOST"), os.Getenv("MYSQL_PORT"), os.Getenv("MYSQL_DB"))
	db, err = gorm.Open("mysql", mysqlConnectString)
	// defer db.Close()
	if err != nil {
		panic("DB Connection Error")
	}

}

func DbManager() *gorm.DB {
	return db
}

func FlushDB() {
	db.Exec("TRUNCATE TABLE movie_actors")
	db.Exec("TRUNCATE TABLE movie_genre")
	db.Exec("TRUNCATE TABLE actors")
	db.Exec("TRUNCATE TABLE genres")
	db.Exec("TRUNCATE TABLE directors")
	db.Exec("TRUNCATE TABLE movies")
}
