module imdb

go 1.12

require (
	github.com/confluentinc/confluent-kafka-go v1.1.0
	github.com/jinzhu/gorm v1.9.11
	github.com/joho/godotenv v1.3.0
	github.com/julienschmidt/httprouter v1.3.0
)
